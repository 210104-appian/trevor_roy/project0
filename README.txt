Getting Started:
Feel free to look at the database to pick a username and password to use.  Under status every user is CUST, EMP, or MGMT.  Any with CUST are customers, any with EMP are employees, and the one with MGMT is the one manager.  If you don't want to look at the database right away below are some credientals to use.  They are case sensitive. 
Login as Customer
	Username:Gambino
	Password:CommunityLover2
Login as Employee:
	Username:Silverhand
	Password:NeverFadeAway
Login as Manager:
	Username:MadMan
	Password:Creative


Tutorials:
Customer
	The customers have three functions they can do.  Apply for a new account, view their accounts, or withdrawl/deposit.
	Apply:
		Input a valid starting amount for an account
	View:
		User does nothing, it automatically displays their accounts
	Withdrawl/Deposit:
		First the user must select an account to access.  From there they choose whether they are withdrawling or depositing.  No matter what they choose the last step is to input the amount they are either withdrawling or depositing.

Employee
	The employees have three functions they can do.  Aprove or reject accounts, view all of a customers accounts, or view all the withdrawls and deposits that have happened
	Check Accounts Waiting Approval:
		The employee chooses an account from the list presented.  Then they either approve, reject, or just backout (in the case they picked the wrong one)
	View Customer Accounts:
		Employee types in the name of a customer they want to see the accounts of
	View Transactions:
		Employee does nothing.  All withdrawls and deposits are shown with the transaction id, user id, users name, account id, and the new balance after the transaction

Manager:
	The manager has 2 functions.  Creating an employee account and creating a customer account.  They are identical functions except for the status.
	Create Employee:
		Manager inputs a name, email, username, and password.  They confirm it before it goes into the database.
	Create Customer:
		 Manager inputs a name, email, username, and password.  They confirm it before it goes into the database.

Dependencies:
3 dependencies were used
	junit:
		<groupId>junit</groupId>
		<artifactId>junit</artifactId>
		<version>4.12</version>
	jdbc:
		<groupId>com.oracle.database.jdbc</groupId>
		<artifactId>ojdbc10</artifactId>
		<version>19.8.0.0</version>
	log4j:
		<groupId>log4j</groupId>
		<artifactId>log4j</artifactId>
		<version>1.2.17</version>

Database Structure:
The database consists of 4 tables
	USERS:
		Stores user information
		Has u_id, name,email,user_name,password,status
		u_id is the Primary Key
		name and email have the NOT NULL constraint
		status has a check constraint so the only allowed values are CUST,EMP,and MGMT
	ACCOUNT:	
		Stores account information
		Has account_id, u_id, and balance.
		account_id is the Primary Key
		u_id is a Foreign Key to the Users table
	WAITING_APPROVAL:
		Stores accounts that customers have applied for, but haven't been approved
		Has account_id, u_id, and start_bal
		account_id is the Primary Key
		u_id has the NOT NULL constraint
		all accounts go through here first before moving to the account table after database creation
	TRANSACTIONS:
		Stores withdrawl and deposit information, not database transactions
		Has transaction_id, user_id, account_id, and new_balance
		transaction_id is the Primary Key
		user_id is a Foreign Key to the users table
		account_id is a Foregin Key to teh account table
		new_balance shows the accounts new balance after the transaction


	