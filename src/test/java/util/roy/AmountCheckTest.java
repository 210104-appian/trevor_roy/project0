package util.roy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AmountCheckTest {
	
	private AmountCheck ac = new AmountCheck();
	@Test
	public void testPositiveNumberGiven() {
		int expected = 1;
		int actual = ac.positiveCheck(9);
		assertEquals(expected,actual);
	}
	
	@Test
	public void testNegativeNumberGiven() {
		int expected = 0;
		int actual = ac.positiveCheck(-9);
		assertEquals(expected,actual);
	}
	
	@Test
	public void testCorrectOverDraft() {
		double expected = 1;
		double actual = ac.overDraft(5, 6);
		assertEquals(expected,actual,.01);
	}
	@Test
	public void testInCorrectOverDraft() {
		double expected = 0;
		double actual = ac.overDraft(5, 4);
		assertEquals(expected,actual,.01);
	}
}
