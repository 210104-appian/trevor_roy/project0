package daos.roy;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import models.roy.Accounts;
import util.roy.ConnectionUtil;

public class CustomerDaoImplTest {
	private CustomerDao custDao = new CustomerDaoImpl();
	
		
	
	@Test
	public void testViewAccountsCustomer() {
		
			int id = 6;
			ArrayList<Integer> accIdList = new ArrayList();
			accIdList.add(3);
			ArrayList<Integer> uIdList = new ArrayList();
			uIdList.add(7);
			ArrayList<Double> balList = new ArrayList();
			balList.add(67.99);
			Accounts expected = new Accounts(accIdList,uIdList,balList);
			double expectedBal = 67.99;
			Accounts actual = custDao.viewAccount(id);
			double actualBal = actual.getBalList().get(0);
			assertEquals(expectedBal,actualBal, .01);
	}
}
