package dev.roy;

import java.util.Scanner;

import org.apache.log4j.Logger;

import users.roy.*;
import util.roy.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Proj0Driver {
	
	private static Logger log = Logger.getRootLogger();
	public static void main(String[] args) {
		String status= "";
		String name = "";
		int id=-1;;
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement();){
			log.info("New connection");

			do{
				Scanner sc = new Scanner(System.in);
				
				System.out.println("Enter Username");
				String username = sc.next();
				System.out.println("Enter Password");
				String password = sc.next();
				log.info("Validating credentials");
				String sql = "SELECT status,name,u_id FROM users WHERE USER_NAME =? AND PASSWORD =?";
				PreparedStatement pState = connection.prepareStatement(sql);
				pState.setString(1, username);
				pState.setString(2, password);
				ResultSet rs = pState.executeQuery();
				
				while(rs.next()) {
					status = rs.getString(1);
					name = rs.getString(2);
					id = rs.getInt(3);
					
				}
				//Search Database for user name
				//Verify that password matches
				//Get user status (Customer or Employee or Manager) and have different menus for each
				switch(status) {
					case "EMP":
						log.info("Validated credentials-employee");
						Employee.employeeMain(name,id);
						break;
					case "CUST":
						log.info("Validated credentials-customer");
						Customer.customerMain(name,id);
						break;
					case "MGMT":
						log.info("Validate credentials-manager");
						Manager.managerMain(name, id);
						break;
					default:
						log.info("Invalid credentials");
						System.out.println("Invalid username and password.  Try Again");
						System.out.println();
						System.out.println("");
				}
					
			}while(status == "");
			
			connection.close();
		}catch(SQLException e) {
			System.out.println("Whoopsie");
			e.printStackTrace();
		}
		
		
	}
}