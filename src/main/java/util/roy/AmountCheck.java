package util.roy;

import java.util.Scanner;

public class AmountCheck {
	
	public int positiveCheck(double depos) {
		if(depos>0) {
			return 1;
		}else {
			return 0;
		}
	}
	
	public int overDraft(double withdrawal,double balance) {
		if(balance-withdrawal<0) {
			return 0;
		}else {
			return 1;
		}
	}

	
}
