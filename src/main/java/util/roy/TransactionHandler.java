package util.roy;

import java.util.Scanner;

import org.apache.log4j.Logger;

public class TransactionHandler {
	public double transactions(double balance) {
		Logger log = Logger.getRootLogger();
		Scanner scan = new Scanner(System.in);
		AmountCheck ac = new AmountCheck();
		int checker;
		int dw = -1;
		do {
			System.out.println("1:Deposit");
			System.out.println("2:Withdrawal");
			
			String reader = scan.next();
			try {
				dw = Integer.parseInt(reader);
			}catch(Exception e) {
				log.error(e.getClass());
				System.out.println("Please enter a valid choice");
				
			}
		
		
		
			if(dw==1) {
				System.out.println("How much are you depositing?");
				
				
				double depos =-1;
				Boolean flag = false;
				do {
				reader = scan.next();
				
					checker = 1;
					try {
						depos = Double.parseDouble(reader);
					}catch(Exception e) {
						log.error(e.getClass());
					}
					
					checker = ac.positiveCheck(depos);
					if(checker == 1) {
						if(balance + depos < 99999999.99) {
							balance = balance +depos;
							flag = true;
						}else {
							System.out.println("Total balance cannot exceed 99999999.99 at this time, please enter an appropriate amount");
							
						}
					}else {
						System.out.println("Please enter a valid amount to deposit");
						
					}
				}while(flag == false);
				break;
					
			}else if(dw == 2) {
				
				
				double withd = -1;
				Boolean flag = false;
					do{
						System.out.println("How much are you withdrawing?");
						reader = scan.next();
					
					try {
						withd = Double.parseDouble(reader);
					}catch(Exception e) {
						log.error(e.getClass());
					}
					
					checker = ac.positiveCheck(withd);
					int od = ac.overDraft(withd, balance);
					if(checker == 1 && od == 1) {
						balance = balance-withd;
						flag = true;
					
					}else {
						if(od == 0) {
							System.out.println("You have insufficent funds");
						}else {
						System.out.println("Please enter a valid amount to withdrawal");
						}
					}
				}while(flag == false);
				break;
			}else {
				System.out.println("Please enter 1 or 2");
			}
			
		}while(dw != 1 || dw != 2 );
		return balance;

	}
}
