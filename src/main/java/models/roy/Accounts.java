package models.roy;

import java.util.ArrayList;

public class Accounts {
	ArrayList<Integer> accIdList = new ArrayList();
	ArrayList<Integer> uIdList = new ArrayList();
	ArrayList<Double> balList = new ArrayList();
	ArrayList<String> nameList = new ArrayList();
	public Accounts(ArrayList<Integer> accIdList, ArrayList<Integer> uIdList, ArrayList<String> nameList, ArrayList<Double> balList) {
		super();
		this.accIdList = accIdList;
		this.uIdList = uIdList;
		this.nameList = nameList;
		this.balList = balList;
	}
	
	public Accounts(ArrayList<Integer> accIdList, ArrayList<Integer> uIdList, ArrayList<Double> balList) {
		super();
		this.accIdList = accIdList;
		this.uIdList = uIdList;
		this.balList = balList;
	}


	public ArrayList<String> getNameList() {
		return nameList;
	}

	public void setNameList(ArrayList<String> nameList) {
		this.nameList = nameList;
	}

	public Accounts(ArrayList<Integer> accIdList, ArrayList<Double> balList) {
		super();
		this.accIdList = accIdList;
		this.balList = balList;
	}



	public ArrayList<Integer> getAccIdList() {
		return accIdList;
	}
	public void setAccIdList(ArrayList<Integer> accIdList) {
		this.accIdList = accIdList;
	}
	public ArrayList<Integer> getuIdList() {
		return uIdList;
	}
	public void setuIdList(ArrayList<Integer> uIdList) {
		this.uIdList = uIdList;
	}
	public ArrayList<Double> getBalList() {
		return balList;
	}
	public void setBalList(ArrayList<Double> balList) {
		this.balList = balList;
	}
	
	
}
