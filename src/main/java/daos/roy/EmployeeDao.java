package daos.roy;

import java.sql.ResultSet;

import models.roy.Accounts;

public interface EmployeeDao {
	public Accounts getAccountsWaiting();
	public void approveAccount(int approvalChoice,int id, double bal);
	public void rejectAccount(int approvalChoice);
	public Accounts viewCustomerAccounts(String name);
	public void viewTransactions();
	
}
