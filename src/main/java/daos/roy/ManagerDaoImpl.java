package daos.roy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import util.roy.ConnectionUtil;

public class ManagerDaoImpl implements ManagerDao {

	private static Logger log = Logger.getRootLogger();
	@Override
	public void createEmployee(String name, String email, String userName, String password) {
		try(Connection connection = ConnectionUtil.getConnection()){
			log.info("Inserting data into USERS");
			PreparedStatement pStatement = connection.prepareStatement("INSERT INTO USERS(NAME ,EMAIL,USER_NAME ,PASSWORD ,STATUS) VALUES(lower(?),?,?,?,?)");
			pStatement.setString(1, name);
			pStatement.setString(2,email);
			pStatement.setString(3,userName);
			pStatement.setString(4,password);
			pStatement.setString(5,"EMP");
			pStatement.executeUpdate();
			log.info("Insert Complete");
		}catch(SQLException e) {
			log.error("Exception: " + e.getClass());
			e.printStackTrace();
		}
		
	}

	@Override
	public void createCustomer(String name, String email, String userName, String password) {
		try(Connection connection = ConnectionUtil.getConnection();){
			log.info("Inserting data into USERS");
			PreparedStatement pStatement = connection.prepareStatement("INSERT INTO USERS(NAME ,EMAIL,USER_NAME ,PASSWORD ,STATUS) VALUES(lower(?),?,?,?,?)");
			pStatement.setString(1, name);
			pStatement.setString(2,email);
			pStatement.setString(3,userName);
			pStatement.setString(4,password);
			pStatement.setString(5,"CUST");
			pStatement.executeUpdate();
			log.info("Insert Complete");
		}catch(SQLException e) {
			log.error("Exception: " + e.getClass());
			e.printStackTrace();
		}


	}
}
