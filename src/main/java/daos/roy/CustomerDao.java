package daos.roy;

import models.roy.Accounts;

public interface CustomerDao {
	
	public void apply(int id,double bal);
	public Accounts viewAccount(int id);
	public void withDepos(int u_id, int acc_id, double bal);
	
}
