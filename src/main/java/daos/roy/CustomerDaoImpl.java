package daos.roy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import org.apache.log4j.Logger;

import models.roy.Accounts;
import util.roy.AmountCheck;
import util.roy.ConnectionUtil;
import util.roy.TransactionHandler;

public class CustomerDaoImpl implements CustomerDao {
	
	private static Logger log = Logger.getRootLogger();
	public void apply(int id,double bal) {
		try(Connection connection = ConnectionUtil.getConnection();){	
			log.info("Inserting data to WAITING_APPROVAL");
			PreparedStatement pstatement = connection.prepareStatement("INSERT INTO WAITING_APPROVAL(U_ID,START_BAL) VALUES (?,?)");
			pstatement.setInt(1, id);
			pstatement.setDouble(2, bal);
			pstatement.executeUpdate();
			log.info("Insert complete");
			System.out.println("Account submitted for approval");
		}catch(SQLException e) {
			log.error("Exception: " +e.getClass());
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Accounts viewAccount(int id) {
		try(Connection connection = ConnectionUtil.getConnection();){
			int ctr = 0;
			ArrayList<Integer> accIdList = new ArrayList();
			ArrayList<Double> balList = new ArrayList();
			log.info("Getting data from Account table");
			PreparedStatement pState = connection.prepareStatement("SELECT ACCOUNT_ID, TO_CHAR(BALANCE,'99999999.000') FROM ACCOUNT WHERE U_ID = ? ");
			pState.setInt(1, id);
			ResultSet rs = pState.executeQuery();
			
			while(rs.next()) {
				accIdList.add(rs.getInt(1));
				balList.add(rs.getDouble(2));
				ctr ++;				
			}
			log.info("Got " + ctr +" rows");
			Accounts acc = new Accounts(accIdList,balList);
			return acc;
		} catch (SQLException e) {
			log.error("Exception: " +e.getClass());
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public void withDepos(int u_id, int acc_id,double bal) {
		try(Connection connection = ConnectionUtil.getConnection();){
				log.info("Updating Balance for account " + acc_id);
				PreparedStatement pState = connection.prepareStatement("UPDATE ACCOUNT SET BALANCE = ? WHERE ACCOUNT_ID = ?");
				pState.setDouble(1, bal);
				pState.setInt(2, acc_id);
				pState.execute();
				log.info("Update complete");
				log.info("Adding record to transactions table");
				pState= connection.prepareStatement("INSERT INTO TRANSACTIONS(USER_ID,ACCOUNT_ID,NEW_BALANCE) VALUES (?,?,?)");
				pState.setInt(1, u_id);
				pState.setInt(2, acc_id);
				pState.setDouble(3,bal);
				pState.executeUpdate();
				log.info("Record added");
		}catch(SQLException e) {
			log.error("Exception: " +e.getClass());
			e.printStackTrace();
	
		}
	}
}
	
