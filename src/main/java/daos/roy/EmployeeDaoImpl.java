package daos.roy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import models.roy.Accounts;
import util.roy.ConnectionUtil;

public class EmployeeDaoImpl implements EmployeeDao {

	private static Logger log = Logger.getRootLogger();
	
	@Override
	public Accounts getAccountsWaiting() {
		try(Connection connection = ConnectionUtil.getConnection();){
			
			ArrayList<Integer> accIdList = new ArrayList();
			ArrayList<Integer> uIdList = new ArrayList();
			ArrayList<Double> balList = new ArrayList();
			ArrayList<String> nameList = new ArrayList();
			log.info("Getting accounts from WAITING_APPROVAL");
			PreparedStatement pState = connection.prepareStatement("SELECT wa.ACCOUNT_ID, wa.U_ID ,u.NAME,wa.START_BAL FROM WAITING_APPROVAL wa,USERS u WHERE u.U_ID = wa.U_ID  ORDER BY ACCOUNT_ID  ");
			ResultSet rs = pState.executeQuery();
			
			while(rs.next()) {
				accIdList.add(rs.getInt(1));
				uIdList.add(rs.getInt(2));
				nameList.add(rs.getString(3));
				balList.add(rs.getDouble(4));						
			}
			Accounts aw = new Accounts(accIdList,uIdList, nameList, balList);
			return aw;

		}catch(SQLException e) {
			
			log.error("Exception: " +e.getClass());
			e.printStackTrace();
			return null;
		}
		
		
	}
	@Override
	public void approveAccount(int approvalChoice,int id, double bal ) {
		try(Connection connection = ConnectionUtil.getConnection();){
			log.info("Approved Account:Moving to ACCOUNT table");
			PreparedStatement pState = connection.prepareStatement("INSERT INTO ACCOUNT VALUES(?,?,?)");
			pState.setInt(1, approvalChoice);
			pState.setInt(2, id);
			pState.setDouble(3,bal);
			pState.execute();
			log.info("INSERT COMPLETE");
			log.info("DELETING from WAITING_APPROVAL");
			pState = connection.prepareStatement("DELETE FROM WAITING_APPROVAL WHERE ACCOUNT_ID = ?");
			pState.setInt(1, approvalChoice);
			pState.execute();
			log.info("DELETE successful");
			System.out.println("Account Approved");
		}catch(SQLException e) {
		
			log.error("Exception: " +e.getClass());
			e.printStackTrace();
		}
	}
	
	public void rejectAccount(int approvalChoice) {
		try(Connection connection = ConnectionUtil.getConnection();){
			log.info("Rejected Account: DELETING from WAITING_APPROVAL");
			PreparedStatement pState = connection.prepareStatement("DELETE FROM WAITING_APPROVAL WHERE ACCOUNT_ID =?");
			pState.setInt(1, approvalChoice);
			pState.execute();
			log.info("DELETE successful");
			System.out.println("Account Rejected");
		}catch(SQLException e) {
		
			log.error("Exception: " +e.getClass());
			e.printStackTrace();
		}
	}
	
	@Override
	public Accounts viewCustomerAccounts(String name) {
		int ctr = 0;
		try(Connection connection = ConnectionUtil.getConnection();){
			ArrayList<Integer> accIdList = new ArrayList();
			ArrayList<Integer> uIdList = new ArrayList();
			ArrayList<Double> balList = new ArrayList();
			
			log.info("Getting info from USERS and ACCOUNT");
			PreparedStatement pState = connection.prepareStatement("SELECT USERS.U_ID,ACCOUNT_ID,BALANCE FROM USERS,ACCOUNT WHERE USERS.U_ID = ACCOUNT.U_ID AND USERS.NAME =LOWER(?)");
			pState.setString(1, name);
			ResultSet rs = pState.executeQuery();
			while(rs.next()) {
				accIdList.add(rs.getInt(1));
				uIdList.add(rs.getInt(2));
				balList.add(rs.getDouble(3));
				ctr++;
			}
			log.info("Got " + ctr + "rows");
			Accounts acc = new Accounts(accIdList, uIdList, balList);
			return acc;
		}catch(SQLException e) {
			log.error("Exception: " +e.getClass());
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public void viewTransactions() {
		try(Connection connection = ConnectionUtil.getConnection();){
			int ctr = 0;
			log.info("Getting data from Transactions table");
			PreparedStatement pState = connection.prepareStatement("SELECT T.TRANSACTION_ID, T.USER_ID,U.NAME,T.ACCOUNT_ID,T.NEW_BALANCE FROM TRANSACTIONS t ,USERS u WHERE T.USER_ID =U.U_ID ORDER BY T.TRANSACTION_ID ");
			ResultSet rs = pState.executeQuery();
			System.out.println("TransactionID   UserID    Name          AccountId       NewBalance");
			while(rs.next()) {
				String tempString = String.format("%.2f", rs.getDouble(5));
				System.out.println(rs.getInt(1)+ "               "+rs.getInt(2)+"         "+rs.getString(3)+"    "+rs.getInt(4)+"             "+ tempString);
				ctr ++;
			}log.info("Got " + ctr + " rows");
		} catch (SQLException e) {
			log.error("Exception: " +e.getClass());
			e.printStackTrace();
		}

		
	}

}
