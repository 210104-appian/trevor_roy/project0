package users.roy;

import java.util.Scanner;

import org.apache.log4j.Logger;

import daos.roy.ManagerDao;
import daos.roy.ManagerDaoImpl;

public class Manager {
	public static void managerMain(String name, int id) {
		Logger log = Logger.getRootLogger();
		Scanner sc = new Scanner(System.in);
		
		String reader;
		String yn;
		
		ManagerDao manDao = new ManagerDaoImpl();
		
		String line = name;
		String upperCase = " ";
		Scanner lineScan = new Scanner(line);
		Boolean flag = false;
		int choice;
		while(lineScan.hasNext()) {
			String word = lineScan.next();
			if(lineScan.hasNext() == false) {
				upperCase += Character.toUpperCase(word.charAt(0)) + word.substring(1);
			}else {
			upperCase += Character.toUpperCase(word.charAt(0)) + word.substring(1) + " ";
			}
		}
		
		do {
			if(flag == false) {
				System.out.println("Welcome "+ upperCase +".  What would you like to do today?");
				flag = true;
			}else {
				System.out.println("Anything else?");
			}
			System.out.println("1:Create Employee Account");
			System.out.println("2:Create Customer Account");
			System.out.println("9:Quit");
			reader = sc.next();
			try {
				choice = Integer.parseInt(reader);
			}catch(Exception e) {
				log.error(e.getClass());
				choice = 8;
			}
			
			switch(choice) {
				case 1:
					System.out.println("Please enter a name");
					sc.nextLine();
					String empName = sc.nextLine();
					System.out.println("Please enter an email");
					String empEmail = sc.nextLine();
					System.out.println("Please enter a username");
					String empUser = sc.nextLine();
					System.out.println("Please enter a password");
					String empPass = sc.nextLine();
					System.out.println("Is this information correct?  Y or N\n");
					System.out.println(empName +" "+ empEmail +" "+  empUser +" "+  empPass);
					flag = false;
					do {
						yn = sc.nextLine();
						if(yn.toUpperCase().equals("Y")) {
							manDao.createEmployee(empName, empEmail, empUser, empPass);
							flag = true;
						}else if(yn.toUpperCase().equals("N")) {
							System.out.println("Taking you to menu");
							flag = true;
						}else {
							System.out.println("Please enter a valid choice");
						}
					}while(flag == false);

					
					break;
				case 2:
					System.out.println("Please enter a name");
					sc.nextLine();
					String cusName = sc.nextLine();
					System.out.println("Please enter an email");
					String cusEmail = sc.nextLine();
					System.out.println("Please enter a username");
					String cusUser = sc.nextLine();
					System.out.println("Please enter a password");
					String cusPass = sc.nextLine();
					System.out.println("Is this information correct?  Y or N\n");
					System.out.println(cusName +" "+ cusEmail +" "+  cusUser +" "+  cusPass);
					flag = false;
					do {
						yn = sc.nextLine();
						if(yn.toUpperCase().equals("Y")) {
							manDao.createCustomer(cusName, cusEmail, cusUser, cusPass);
							flag = true;
						}else if(yn.toUpperCase().equals("N")) {
							System.out.println("Taking you to menu");
							flag = true;
						}else {
							System.out.println("Please enter Y or N");
						}
					}while(flag == false);
					break;
				case 9:
					System.out.println("Goodbye");
					break;
				default:
					System.out.println("Please enter a valid choice");
					break;
			
			
			
			}
		}while(choice != 9);
		
		
	}
}
