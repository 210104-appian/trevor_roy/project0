package users.roy;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;

import daos.roy.EmployeeDao;
import daos.roy.EmployeeDaoImpl;
import models.roy.Accounts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class Employee {
	
	public static void employeeMain(String name,int id){
		//this should be a database grab for the name
		Logger log = Logger.getRootLogger();
		Scanner sc = new Scanner(System.in);
		int choice;
		String reader;
		
		String line = name;
		String upperCase = " ";
		Scanner lineScan = new Scanner(line);
		while(lineScan.hasNext()) {
			String word = lineScan.next();
			if(lineScan.hasNext() == false) {
				upperCase += Character.toUpperCase(word.charAt(0)) + word.substring(1);
			}else {
			upperCase += Character.toUpperCase(word.charAt(0)) + word.substring(1) + " ";
			}
		}
		
		Boolean flag = false;
		EmployeeDao empDao = new EmployeeDaoImpl();
		
		
		do {
			if(flag == false) {
				System.out.println("Welcome "+ upperCase +".  What would you like to do today?");
				flag = true;
			}else {
				System.out.println("Anything else?");
			}
			
			System.out.println("1:Check Accounts Waiting Approval");
			System.out.println("2:View Customer Accounts");
			System.out.println("3:View Transactions");
			System.out.println("9:Quit");
			reader = sc.next();
			try {
				choice = Integer.parseInt(reader);
			}catch(Exception e) {
				log.error(e.getClass());
				choice = 8;
			}
			
			switch(choice) {
				case 1:
					Accounts aw = empDao.getAccountsWaiting();	
					System.out.println("AccountID   UserID    Name            Balance");
					for(int i=0;i<aw.getAccIdList().size();i++) {
						String tempString = String.format("%.2f", aw.getBalList().get(i));
						System.out.println(aw.getAccIdList().get(i)+"            "+aw.getuIdList().get(i)+"         "+ aw.getNameList().get(i) +"  "+ tempString);//aw.getBalList().get(i));
					}
					log.info("Got " + aw.getAccIdList().size()+  " records from WAITING_APPROVAL table");
					if(aw.getAccIdList().isEmpty()) {
						System.out.println("No accounts are waiting approval");
					}else {
						System.out.println("What account are you selecting?  Please use account id.  Type -1 for none");
						int approvalChoice = -2;
						
						reader = sc.next();
						try {
						approvalChoice = Integer.parseInt(reader);
						}catch(Exception e) {
							System.out.println("Invalid choice:Taking you to menu");
							log.error(e.getClass());
							break;
						}
						if(approvalChoice == -1) {
							System.out.println("Taking you to menu");
							break;
						}
						if(aw.getAccIdList().contains(approvalChoice)) {
							int arChoice = -2;
							while(arChoice != 1 || arChoice !=2 ||  arChoice !=3){
							int index = aw.getAccIdList().indexOf(approvalChoice);
							
							System.out.println("1:Approve or 2:Reject or 3:Quit ?");
							
							
							reader = sc.next();
							try {
							arChoice = Integer.parseInt(reader);
							}catch(Exception e) {
								
								log.error(e.getClass());
								
							}
							
							int idChoice = aw.getuIdList().get(index);
							double balance = aw.getBalList().get(index);
							
							if(arChoice == 1) {
								empDao.approveAccount(approvalChoice,idChoice,balance);
								break;
							}else if(arChoice == 2) {
								empDao.rejectAccount(approvalChoice);
								break;
							}else if(arChoice == 3){
								System.out.println("Taking you out to main menu");
								break;
							}else  {
								System.out.println("Please pick a valid option");
							}
							
							}
						}else {
							System.out.println("Please try again with a valid option.  Taking you to menu");
						}
					}
				
					break;
				case 2:
					
					System.out.println("Whose account do you want to view?");
					sc.nextLine();
					String nameChoice = sc.nextLine();
					Accounts acc = empDao.viewCustomerAccounts(nameChoice);
					if(acc.getAccIdList().size()>0) {
						System.out.println("AccountID   UserID    Balance");
						for(int i=0;i<acc.getAccIdList().size();i++) {
						String tempString = String.format("%.2f", acc.getBalList().get(i));
						System.out.println(acc.getAccIdList().get(i)+"           "+acc.getuIdList().get(i)+"         "+tempString);
					}
					}else {
						System.out.println("No accounts with that name");
					}

					break;
				case 3:
					empDao.viewTransactions();
					break;
				case 9:
					System.out.println("Goodbye");
					break;
				default:
					System.out.println("Please enter a valid choice");
					break;
					
			}
			
		}while(choice != 9);
	}
}