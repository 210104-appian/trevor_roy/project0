package users.roy;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;

import daos.roy.CustomerDao;
import daos.roy.CustomerDaoImpl;
import models.roy.Accounts;
import util.roy.*;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Customer  {
	
	public static void customerMain(String name, int id) {
		CustomerDao custDao = new CustomerDaoImpl();
		Logger log = Logger.getRootLogger();
		Scanner sc = new Scanner(System.in);
		String reader;
		double newBal = 0;
		int choice;
		Boolean flag = false;
		
		String line = name;
		String upperCase = " ";
		Scanner lineScan = new Scanner(line);
		while(lineScan.hasNext()) {
			String word = lineScan.next();
			if(lineScan.hasNext() == false) {
				upperCase += Character.toUpperCase(word.charAt(0)) + word.substring(1);
			}else {
			upperCase += Character.toUpperCase(word.charAt(0)) + word.substring(1) + " ";
			}
		}
		
		do {
		if(flag == false) {
			System.out.println("Welcome "+ upperCase + ".  What would you like to do today?");
			flag = true;
		}else {
			System.out.println("Anything else?");
		}
		
		System.out.println("1:Apply");
		System.out.println("2:View account");
		System.out.println("3:Withdrawal/Deposit");
		System.out.println("9:Quit");
		
		try {
			reader = sc.next();
			choice = Integer.parseInt(reader);
			}catch(Exception e) {
				log.error(e.getClass());
				choice = 8;
			}
		
//		choice = sc.nextInt();
		switch(choice) {		
			case 1:
				System.out.println("What will the starting balance be?");
				reader = sc.next();
				try {
				newBal = Double.parseDouble(reader);
				}catch(Exception e) {
					log.error(e.getClass());
				}
				if(newBal < 1 || newBal>99999999.99) {
					System.out.println("Please try again with a valid amount");
				}else {
					custDao.apply(id,newBal);
				}
				break;
			case 2:
				Accounts acc = custDao.viewAccount(id);
				System.out.println("AccountID  Balance");
				for(int i=0;i<acc.getAccIdList().size();i++) {
					String tempString = String.format("%.2f", acc.getBalList().get(i));
					System.out.println(acc.getAccIdList().get(i)+"          " + tempString);
				}
				if(acc.getAccIdList().size() == 0) {
					System.out.println("You have no active accounts to view");
				}
				break;
			case 3:
				Accounts tAcc = custDao.viewAccount(id);
				double updBal;
				TransactionHandler th = new TransactionHandler();
				if(tAcc.getAccIdList().size()>0) {
					System.out.println("AccountID  Balance");
					for(int i=0;i<tAcc.getAccIdList().size();i++) {
						String tempString = String.format("%.2f", tAcc.getBalList().get(i));
						System.out.println(tAcc.getAccIdList().get(i)+"          " + tempString);
					}
					int accChoice = -1;
					do {
						
						System.out.println("What account do you want to access today?");
					
						reader = sc.next();
						try {
						accChoice = Integer.parseInt(reader);
						}catch(Exception e) {
							log.error(e.getClass());
							System.out.println("Please enter a valid choice");
							
						}
						if(tAcc.getAccIdList().contains(accChoice) == false) {
							System.out.println("Please enter a valid choice");
						}
						
					}while(tAcc.getAccIdList().contains(accChoice) == false);
					int index = tAcc.getAccIdList().indexOf(accChoice);
					updBal = th.transactions(tAcc.getBalList().get(index));
					custDao.withDepos(id, accChoice,updBal);
					
				}else {
					System.out.println("You have no active accounts right now to view");
				}
				
				
				break;
			case 9:
				System.out.println("Goodbye");
				break;
			default:
				System.out.println("Please enter a valid choice");
				break;
		}		
				
		}while(choice != 9);
	}
	
	
}
